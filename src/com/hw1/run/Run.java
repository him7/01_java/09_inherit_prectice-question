package com.hw1.run;

import java.util.Scanner;

import com.hw1.model.dto.*;

public class Run {
	
	public static void main(String[] args) {
		System.out.println("1. ======================================================================");
		Student[] studentArr = new Student[] {
				new Student("홍길동", 20, 178.2, 70.0, 1, "정보시스템공학과"),
				new Student("김말똥", 21, 187.3, 80.0, 2, "경영학과"),
				new Student("강개순", 23, 167.0, 45.0, 4, "정보통신공학과")
				};
		
		for(Student sd : studentArr) {
			System.out.println(sd.information());
		}
		
		System.out.println("2. ======================================================================");
		Employee[] employeeArr = new Employee[10];
		Scanner sc = new Scanner(System.in);
		int cnt = 0;
		while(true) {
			System.out.print("사원 이름 : ");
			String name = sc.next();
			System.out.print("사원 나이 : ");
			int age = sc.nextInt();
			System.out.print("사원 신장 : ");
			double height = sc.nextDouble();
			System.out.print("사원 몸무게 : ");
			double weight = sc.nextDouble();
			System.out.print("사원 급여 : ");
			int salary = sc.nextInt();
			System.out.print("사원 부서 : ");
			String dept = sc.next();
			
			employeeArr[cnt] = new Employee(name, age, height, weight, salary, dept);
			
			System.out.print("사원 추가(y&Y) : ");
			char insert = Character.toUpperCase(sc.next().charAt(0));
			if(insert == 'Y') {
				cnt++;
				continue;
			} else {
				for(Employee ey : employeeArr) {
					if(ey != null) System.out.println(ey.information());
				}
				cnt++;
			}
			if(cnt==10) {
				System.out.println("사원 10명까지 입력 가능합니다. 저장된 사원들 정보 초기화합니다.");
				cnt=0;
			}
		}
		
		
		
		
		
		
		
		
	}
}
